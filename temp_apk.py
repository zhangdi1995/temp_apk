#encoding=utf-8
from flask import Flask,request,render_template,send_from_directory,redirect,flash
from flask.ext.script import Manager
from flask.ext.bootstrap import Bootstrap
import os
app = Flask(__name__)
app.debug=True
app.config['SECRET_KEY']='qweqwe'
bootstrap=Bootstrap(app)
manager = Manager(app)

@app.route('/apk_files/<filename>',methods=['GET',"POST"])
def apk_files(filename):
    return send_from_directory('static',
                               filename, as_attachment=True)
@app.route('/',methods=['GET',"POST"])
def list():
    filenames=[]
    for filename in os.listdir('static'):
        filenames.append(filename)

    return render_template('list.html',filenames=filenames)
@app.route('/upload',methods=['GET',"POST"])
def upload():
    if request.method=='POST':
        apk_file=request.files.get('file')
        apk_title=request.form.get('title')
        print apk_file,apk_title
        if apk_title and apk_file and apk_title not in os.listdir('static'):
            apk_file.save('static/'+apk_title)
        else:
            flash(u'必须要有文件,文件名,且文件名不与之前的文件重复')
    return render_template('upload.html')

if __name__ == '__main__':
    manager.run()
